package plugins.ylemontag.mathoperationspp;

import icy.system.IcyHandledException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import plugins.adufour.ezplug.EzVar;
import plugins.adufour.ezplug.EzVarListener;
import plugins.adufour.vars.gui.model.VarEditorModel;
import plugins.ylemontag.mathoperations.Expression;
import plugins.ylemontag.mathoperations.Functor;
import plugins.ylemontag.mathoperations.MathOperationAbstractPlugin;
import plugins.ylemontag.mathoperations.VarExpression;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Plugin for complex math operations
 */
public class MathOperationPPPlugin extends MathOperationAbstractPlugin
{
	private EzVarExpression             _expression = new EzVarExpression("Operation", null);
	private Map<String, EzGroupVariant> _inputs     = new HashMap<String, EzGroupVariant>();
	
	/**
	 * Wrap VarExpression into the EzVar structure
	 */
	private static class EzVarExpression extends EzVar<Expression>
	{
		/**
		 * Constructor
		 */
		public EzVarExpression(String name, Expression defaultValue)
		{
			super(new VarExpression(name, defaultValue), new VarEditorModel<Expression>()
			{
				@Override
				public boolean isValid(Expression value) {
					return true;
				}
				
				@Override
				public Expression getDefaultValue() {
					return null;
				}
			});
		}
	}
	
	/**
	 * Refresh the list of visible inputs
	 */
	private void refreshInputs(Expression expr)
	{
		Set<String> currentVariables = new HashSet<String>();
		if(expr!=null) {
			for(String s : expr.getVariables()) {
				EzGroupVariant group = _inputs.get(s);
				if(group==null) {
					group = new EzGroupVariant(s);
					_inputs.put(s, group);
					addEzComponent(group);
				}
				else if(!group.isVisible()) {
					group.setVisible(true);
				}
				currentVariables.add(s);
			}
		}
		for(String s : _inputs.keySet()) {
			if(!currentVariables.contains(s)) {
				_inputs.get(s).setVisible(false);
			}
		}
	}
	
	@Override
	protected void initialize()
	{
		addEzComponent(_expression);
		_expression.addVarChangeListener(new EzVarListener<Expression>()
		{
			@Override
			public void variableChanged(EzVar<Expression> source, Expression newValue)
			{
				refreshInputs(newValue);
			}
		});
	}
	
	@Override
	protected void execute()
	{
		// Retrieve the functor
		Functor    fun  = null;
		Expression expr = _expression.getValue();
		if(expr!=null) {
			try {
				fun = expr.getFunctor();
			}
			catch(Expression.BadFunctor err) {}
		}
		if(fun==null) {
			throw new IcyHandledException("The 'Operation' field is either empty or inconsistent.");
		}
		
		// Collect the inputs
		String        [] inputNames = expr.getVariablesAsArray();
		EzGroupVariant[] inputs     = new EzGroupVariant[inputNames.length];
		for(int k=0; k<inputs.length; ++k) {
			inputs[k] = _inputs.get(inputNames[k]);
		}
		
		// Call the core function
		coreExecute(fun, inputs);
	}
}
