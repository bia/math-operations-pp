package plugins.ylemontag.mathoperationspp;

import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.util.VarException;
import plugins.ylemontag.mathoperations.Expression;
import plugins.ylemontag.mathoperations.Functor;
import plugins.ylemontag.mathoperations.MathOperationAbstractBlock;
import plugins.ylemontag.mathoperations.VarExpression;
import plugins.ylemontag.mathoperations.Variant;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Blocks with text-defined expressions
 */
public class MathOperationPPBlocks
{
	/**
	 * Generic block
	 */
	private static class AbstractBlock extends MathOperationAbstractBlock
	{
		private String[]      _names     ;
		private VarExpression _expression;
		
		protected AbstractBlock(String[] variables)
		{
			super("result");
			_names      = variables;
			_expression = new VarExpression("op", null);
		}

		@Override
		public void declareInput(VarList inputMap)
		{
			inputMap.add(_expression);
			for(String name : _names) {
				addInputVariant(inputMap, name, name);
			}
		}
		
		@Override
		public String getMainPluginClassName()
		{
			return MathOperationPPPlugin.class.getName();
		}
		
		@Override
		public String getName()
		{
			return "Math expression (" + _names.length + ")";
		}
		
		@Override
		public void run()
		{
			// Retrieve the input values
			Functor   fun    = extractFunctor();
			Variant[] in     = new Variant[_names.length];
			String [] inDesc = new String [_names.length];
			for(int k=0; k<_names.length; ++k) {
				in    [k] = retrieveInputValue(_names[k]);
				inDesc[k] = in[k].toString();
			}
			String description = fun.describeOperation(inDesc);
			
			// Execute the operation and save the value
			try {
				Variant out = fun.apply(in);
				defineOutputValue(out);
			}
			catch(Functor.InconsistentArguments err) {
				reportError(err, description);
			}
		}
		
		/**
		 * Extract the functor
		 */
		private Functor extractFunctor()
		{
			Expression expression = _expression.getValue();
			if(expression==null) {
				throw new VarException("The 'op' field is either empty or inconsistent.");
			}
			try {
				return expression.getFunctor(_names);
			}
			catch(Expression.BadFunctor err) {
				throw new VarException(
					"Cannot interpret the 'op' field. Here is the error message: " + err.getMessage()
				);
			}
		}
	}
	
	/**
	 * 1 argument block
	 */
	public static class MathExpression1 extends AbstractBlock
	{
		public MathExpression1()
		{
			super(new String[] {"a"});
		}
	}
	
	/**
	 * 2 argument block
	 */
	public static class MathExpression2 extends AbstractBlock
	{
		public MathExpression2()
		{
			super(new String[] {"a", "b"});
		}
	}
	
	/**
	 * 3 argument block
	 */
	public static class MathExpression3 extends AbstractBlock
	{
		public MathExpression3()
		{
			super(new String[] {"a", "b", "c"});
		}
	}
	
	/**
	 * 4 argument block
	 */
	public static class MathExpression4 extends AbstractBlock
	{
		public MathExpression4()
		{
			super(new String[] {"a", "b", "c", "d"});
		}
	}
}
